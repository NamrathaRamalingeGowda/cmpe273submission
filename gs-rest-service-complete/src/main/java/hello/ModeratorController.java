package hello;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod.*;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@EnableWebMvcSecurity
@RequestMapping(value="/api/v1")
@RestController
public class ModeratorController extends WebSecurityConfigurerAdapter {
	
	
	Moderator mode = new Moderator();
	Polls poll = new Polls();
	
	ArrayList <Moderator> strlist1 = new ArrayList<Moderator>();
	ArrayList <Polls> strlist2 = new ArrayList<Polls>();

	private static final AtomicLong counter = new AtomicLong(123455);
	private SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
     int [] temp1 = new int[2];
     int [] result = new int[2];
     int [] temp2= new int[2];
	 String [] choice = new String[2];
    
	 protected void configure(HttpSecurity http) throws Exception {
                http
                .httpBasic().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/api/v1/moderators").permitAll()
                .antMatchers("/api/v1/polls/**").permitAll()
                .antMatchers("/api/v1/moderators/**").fullyAuthenticated().anyRequest().hasRole("USER");
            }

         @Autowired
            public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
                auth
                    .inMemoryAuthentication()
                        .withUser("foo").password("bar").roles("USER");
            }
    
    
    
    
	@RequestMapping(value = "/moderators", method = RequestMethod.POST)
	
	public ResponseEntity <Moderator> moderator(@Valid @RequestBody Moderator mode) {
		
		String date = new Date().toString();	
		mode.setCreated_at(formater.format(new Date()));
		mode.setId((int)counter.incrementAndGet());
		strlist1.add(mode);
		
		return new ResponseEntity<Moderator>(mode,HttpStatus.CREATED);
		
	   }

	@RequestMapping(value = "/moderators/{id}", method = RequestMethod.GET)
		public ResponseEntity <Moderator> Viewmoderator(@PathVariable int id) {
	
		int l = 0;
		
		System.out.println("size is"+strlist1.size());
		
		for(int i=0;i<strlist1.size();i++)
		{
			if(id == strlist1.get(i).getId())
			{
				l=i;
			}
		}
		
		return new ResponseEntity<Moderator>(strlist1.get(l),HttpStatus.OK);
	   
	}
	
	@RequestMapping(value = "/moderators/{id}", method = RequestMethod.PUT)
	 public ResponseEntity <Moderator> updatemoderator(@Valid @RequestBody Moderator mode,@PathVariable int id) {
		
         int l = 0;
		
         String email = mode.getEmail();
		 String password= mode.getPassword();
		
		System.out.println("size is"+strlist1.size());
		for(int i=0;i<strlist1.size();i++)
		{
			if(id == strlist1.get(i).getId())
			{
			
				l=i;
				strlist1.get(i).setEmail(email);
				strlist1.get(i).setPassword(password);
				
			}
			
		}
		return new ResponseEntity<Moderator>(strlist1.get(l),HttpStatus.OK);
	   }
	
    @RequestMapping(value = "/moderators/{moderator_id}/polls", method = RequestMethod.POST)

	public ResponseEntity <Polls> createPoll(@Valid @RequestBody Polls poll,@PathVariable int moderator_id) {
		
    	poll.setId(Integer.toString((int) counter.incrementAndGet(),36));
    	
    	strlist2.add(poll);
    
    	
		for(int i=0;i<strlist1.size();i++)
		{
			if(moderator_id == strlist1.get(i).getId())
			{
				strlist1.get(i).getPollslist().add(poll);
		
			}
			
		}
	
		return new ResponseEntity<Polls>(poll,HttpStatus.CREATED);
		
	   }
   
  

	@RequestMapping(value = "/polls/{poll_id}", method = RequestMethod.GET)
		
	    public ResponseEntity <Polls> viewPollsWithoughResult(@PathVariable String poll_id) {
	
		int l = 0;
		
		System.out.println("size is"+strlist2.size());
		
		for(int i=0; i<strlist2.size(); i++)
		{
			if(poll_id.equals(strlist2.get(i).getId()))
			{
				l=i;
			}
		}
		
		return new ResponseEntity<Polls>(strlist2.get(l),HttpStatus.OK);
	}

	@RequestMapping(value = "/moderators/{moderator_id}/polls/{poll_id}", method = RequestMethod.GET)
	public ResponseEntity viewPollWithResult(@PathVariable int moderator_id,@PathVariable String poll_id) {

    int l = 0;
	System.out.println("size is"+strlist1.size());
	
	for(int i=0;i<strlist1.size();i++)
	{
		if(moderator_id == strlist1.get(i).getId())
		{
		
			for(int j=0;j<strlist2.size();j++)
			{
				if(poll_id.equals(strlist2.get(j).getId()))	
				{
					return new ResponseEntity(strlist1.get(i).getPollslist().get(j),HttpStatus.OK);
				}
			}
		}	
	
	}
	
     return new ResponseEntity("View Polls is not sucessfull",HttpStatus.OK);
}

	@RequestMapping(value = "/moderators/{moderator_id}/polls", method = RequestMethod.GET)
	public ResponseEntity listAllPolls(@PathVariable int moderator_id) {

    int l = 0;
	System.out.println("size is"+strlist1.size());
	
	for(int i=0;i<strlist1.size();i++)
	{
		if(moderator_id == strlist1.get(i).getId())
		{
		
					return new ResponseEntity(strlist1.get(i).getPollslist(),HttpStatus.OK);
		}	
	}
     return new ResponseEntity("View Polls is not sucessfull",HttpStatus.OK);
}

	
	
	@RequestMapping(value = "/moderators/{moderator_id}/polls/{poll_id}", method = RequestMethod.DELETE)
	public ResponseEntity deletePoll(@PathVariable int moderator_id,@PathVariable String poll_id) {

    int l = 0;
	System.out.println("size is"+strlist1.size());
	
	for(int i=0;i<strlist1.size();i++)
	{
		if(moderator_id == strlist1.get(i).getId())
		{
		
			for(int j=0;j<strlist2.size();j++)
			{
				if(poll_id.equals(strlist2.get(j).getId()))	
				{
					strlist2.remove(j);
					return new ResponseEntity(strlist1.get(i).getPollslist(),HttpStatus.NO_CONTENT);
				}
			}
		}	
	
	}
	
     return new ResponseEntity("Delete Polls is not sucessfull",HttpStatus.OK);
}

	
	
	 @RequestMapping(value = "/polls/{poll_id}", method = RequestMethod.PUT)
	 public ResponseEntity voteAPoll(@PathVariable String poll_id,@RequestParam(value="choice")int choice_index) 
	 {
		 for(int i=0;i<strlist2.size();i++)
		    {
			  if(poll_id.equals(strlist2.get(i).getId()))
			  {
			   
				  	if(choice_index == 0)
				  		{
				  		
				  		temp1=strlist2.get(i).getResult();
				  		temp1[choice_index]=temp1[choice_index]+1;	
				  		strlist2.get(i).setResult(temp1);
				  	 	return new ResponseEntity(HttpStatus.NO_CONTENT);
				  		
				  		}
				  	else if(choice_index==1)
		            {
				  		 temp1=strlist2.get(i).getResult();
				  		 temp1[choice_index]=temp1[choice_index]+1;
						 strlist2.get(i).setResult(temp1);
						 return new ResponseEntity(HttpStatus.NO_CONTENT);
		            }
				  	
    		    }
            }
		    	 
           	return new ResponseEntity("Not able to vote",HttpStatus.NO_CONTENT);
		
	   }

     @ExceptionHandler(MethodArgumentNotValidException.class)
     @ResponseBody
     public ResponseEntity handleBadInput(MethodArgumentNotValidException e)
     {
         String errors="";
         for(FieldError obj: e.getBindingResult().getFieldErrors())
             {
                 errors+=obj.getDefaultMessage();
             }    
         return new ResponseEntity(errors,HttpStatus.BAD_REQUEST);
     }
}
