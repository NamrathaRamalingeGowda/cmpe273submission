package demo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import beans.Poll;

@Component
public class Scheduler
{

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	SimpleProducer producer;
	@Autowired
	SMSUtil utility;
	Poll poll;

    @Scheduled(fixedRate = 300000) //5 minutes 300000 miliseconds
    public void mailScheduler() 
    {
    	
    	ArrayList<Poll> expPoll = new ArrayList<Poll>();
        producer= new SimpleProducer();
        expPoll = utility.pollLookUp();  
        System.out.println("List of Expired Polls: "+expPoll);
        
        if(!expPoll.isEmpty())
        {
        	for(int i=0; i<expPoll.size();i++)
        	{	
        		if(expPoll.get(i).isFlag() != true)
				{
        			Poll temppoll= expPoll.get(i);
                    String[] choice= temppoll.getChoice();
                    Integer[] results =temppoll.getResults();
        			//producer.kafka("cmpe273-topic", expPoll.get(i).getModerator().getEmail() + ":010096268:Poll Result [Android=100,iPhone=200]");
                    producer.kafka("cmpe273-new-topic", expPoll.get(i).getModerator().getEmail() + ":010096268:Poll Result [" + choice[0] + "=" +results[0]+","+choice[1]+"="+results[1]+"]");
                            System.out.println("Condition for poll expiration satisfied");
        			poll = expPoll.get(i);
        			poll.setFlag(true);
        			utility.UpdatePoll(poll);
        			System.out.println();
					
				}
        	}	
        }
    }  
}

